Categories:Phone & SMS
License:GPLv3+
Web Site:https://github.com/MrDoomy/OverFlow/blob/HEAD/README.md
Source Code:https://github.com/MrDoomy/OverFlow
Issue Tracker:https://github.com/MrDoomy/OverFlow/issues

Auto Name:OverFlow
Summary:Send SMS in bulk
Description:
Send SMS in bulk. Features include:

* Retrive mobile phone numbers of contacts
* Send messages
* Choose the number of messages to send
* Be notified of the number of messages sent
.

Repo Type:git
Repo:https://github.com/MrDoomy/OverFlow

Build:1.0,1
    commit=2d201b3a5d14eb3b9797816384d491817a5733da
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1

